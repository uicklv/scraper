<?php


namespace App;
use DOMDocument;

class Parser
{

    protected $url;
    protected $path;
    protected $htmlContent;

    public function __construct($arvg)
    {
        /*
        получаем в конструктор параметры переданные скрипту,
        проверяем их кол-во и устанавливаем урлу и путь создания файла
        */
        if (count($arvg) == 3)
        {
            $this->setUrl($arvg[1]);
            $this->setPath($arvg[2]);
            $this->setHtmlContent();
        }

    }

    protected function setUrl($url)
    {
        $this->url = $url;
    }

    protected function setPath($path)
    {
        $this->path = $path;
    }

    protected function setHtmlContent()
    {
        //Получаем содерживое страницы переданой скрипту
        $c = curl_init($this->getUrl());
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $this->htmlContent = curl_exec($c);
        curl_close($c);

    }

    protected function getUrl()
    {
        return $this->url;
    }

    protected function getPath()
    {
        return $this->path;
    }

    protected function getHtmlContent()
    {
        return $this->htmlContent;
    }

    public function getResult()
    {

        /*
         получаем контент страницы , находим кол-во картинок и ссылок на странице.
         Получаем время выполнения скрипта.
         Выводим в консоль результат.
         */
        $doc = new DOMDocument();
        @$doc->loadHTML($this->getHtmlContent());
        $amountImg = $doc->getElementsByTagName( 'img' );
        $amountLink = $doc->getElementsByTagName( 'a' );
        $start = microtime(true);
        //Вызываем метод для создания CSV файла
        $this->createCsv($this->fullPathImg($amountImg), $this->fullPathLink($amountLink));

        echo 'Total images: ' . $amountImg->length . ' | Total links: ' . $amountLink->length , PHP_EOL
         . 'Time: ' . round(microtime(true) - $start, 4) . ' seconds' . PHP_EOL;

    }

    protected function fullPathImg($amountImg)
    {

        //Парсим страницу , получаем scheme и host
        $parts = parse_url($this->getUrl());
        if ($parts['scheme'] && $parts['host'])
        {
            $base_url = $parts['scheme'] . '://' . $parts['host'] . '/';
        }
        //Проверям path картинок, если он содержит '..' убираем и дописываем полный путь к картинке
        $all_path = [];
        foreach ($amountImg as $image) {

            //получаем значение атрибута 'src'
            $path = $image->getAttribute('src');
            $array_path = explode('/', $path);
            if (in_array('..', $array_path)) {
                $path_new = str_replace('../', '', $path);
                array_push($all_path, $base_url . $path_new);
            }else {
                array_push($all_path, $image->getAttribute('src'));
            }
        }

        return $all_path;
    }

    public function fullPathLink($amountLink)
    {
        //Получаем значение атрибута href каждой ссылки и заносим в массив
        $all_path = [];
        foreach ($amountLink as $link)
        {
            array_push($all_path, $link->getAttribute('href'));
        }

        return $all_path;
    }

    protected function createCsv(array $imgs, array $links)
    {

        //Готовим данные для занесения в CSV файл.
        $data_csv = [];

        //перебор  url картинок и занесение в общий массив
        foreach ($imgs as $img)
        {
            $data_csv[] = [date('Y-m-d'), $this->getUrl(), 'img', $img];
        }

        //перебор  url ссылок и занесение в общий массив
        foreach ($links as $link)
        {
            $data_csv[] = [date('Y-m-d'), $this->getUrl(), 'link', $link];
        }

        //Создаём csv файл по указаному пути и заносим в него данные.
        $fp = fopen($this->getPath(), 'w');

        foreach ($data_csv as $fields) {
            fputcsv($fp, $fields,';');
        }

        fclose($fp);
    }

}